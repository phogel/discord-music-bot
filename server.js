'use_strict';
//Import requires
var Discord         = require('discord.js');
var config          = require('config');
var ytdl            = require('ytdl-core');
var youtubeUrl      = require('youtube-url');
var search          = require('youtube-search');
var async           = require('async');
var prettyMs        = require('pretty-ms');
var bot             = new Discord.Client({autoReconnect: true});

//Import config vars
var serverName      = config.get('Default.serverName');
var channelName     = config.get('Default.channelName');
var textChannelName = config.get('Default.textChannelName');
var gamePlaying     = config.get('Default.gamePlaying');
var ytdlOpts        = config.get('ytdlOptions');
var ytSearchOpts    = config.get('youtubeSearchOpts');
var discordKey      = config.get('Keys.discord');
var commandPrefix   = config.get('Default.prefix');
var radio           = config.get('Radio');
var queue           = [];
var stopped         = true;
bot.loginWithToken(discordKey);

bot.on('ready', function() {
  connect();
});

bot.on('message', function(message) {
if(message.author.id !== bot.user.id &&
   message.server !== null &&
   message.channel.name === textChannelName &&
   message.content[0] === commandPrefix){
  commandChecker(message, message.content.substring(1));
  console.log(new Date().toISOString() + ',  '+ message.author.name +
  ' -> '+ message.content);
}
});

function commandChecker(message, content){
  var params = content.split(' ');
  switch (params[0]) {
    case 'play':
      getYoutubeUrl( message, params );
      break;
    case 'radio':
      playRadio(message, params);
      break;
    case 'vol':
      vol(message, params);
      break;
    case 'stop':
      stop(params);
      break;
    case 'resume':
      resume(params);
      break;
    case 'next':
      next(params);
      break;
    case 'list':
      list(params);
      break;
    default:
      bot.sendMessage(message.channel, 'invalid command');
      break;
  }
}

function addSong( message, videoUrl ) {
  ytdl.getInfo(videoUrl, ytdlOpts, function(err, info){
    if(err) {return console.log(err);}
    var added = false;
    async.forEachOf(info.formats, function (value, key, callback) {
      if(value.bitrate === null && value.audioBitrate <= 128 && !added){
        queue.push({
          title: info.title,
          user: message.author.username,
          url: value.url
        });
        console.log(value.url);
        added = true;
      }
    }, function (err) {

    });
    bot.sendMessage(message.channel,':heavy_plus_sign: `Added` 【**' +
    info.title +
    '**】 on pos `' + queue.length + '`');
    stopped = false;
    if(!stopped && queue.length !== 0 && !bot.voiceConnection.playing){
      playNextSong();
    }
  });
}

function listSongs() {
  var tempArray = [];
  tempArray.push('🎵 **Playlist** 🎵\n');
  if(queue.length){
    async.forEachOf(queue, function (value, key, callback) {
      var pos = key + 1;
      console.log('#' + pos +'  '+value.title);
      tempArray.push('**`#' + pos +'`**  【**'+value.title +
       '**】 Added by: **' + value.user + '**\n');
      callback();
    }, function (err) {
        if (err) {console.error(err.message);}
    });
  }else{
    tempArray.push('**No more songs in playlist!**');
  }
  return tempArray.join('').concat();
}

function getYoutubeUrl( message, params ){
  if(youtubeUrl.valid(params[1])) {
    addSong(message, params[1]);
  }else{
    params.shift();
    search(params.join(' '), ytSearchOpts, function(err, results) {
      if(err) {return console.log(err);}
      addSong(message, results[0].link);
    });
  }
}

function vol( message, params ) {
  if( !isNaN(params[1]) && params[1] <= 100) {
    var volume = params[1]/100;
    bot.sendMessage(message.channel, ':loud_sound: Volume set to `'+
    params[1]+'%`');
    bot.voiceConnection.setVolume(volume);
  }
}

function stop( params ) {
  bot.voiceConnection.stopPlaying();
  stopped = true;
}

function resume( params ) {
    stopped = false;
}

function next( params ) {
  if(!stopped && queue.length !== 0 && bot.voiceConnection.playing){
    bot.voiceConnection.stopPlaying();
  }
}

function list( params ) {
  bot.sendMessage(bot.channels.get('name', textChannelName), listSongs());
}

function connect() {
  var channel = bot.servers.get('name', serverName)
  .channels.get('name', channelName);
  bot.joinVoiceChannel(channel, function(error) {
    console.log(error.message);
  });
  bot.setStatusOnline();
  if (gamePlaying) {
    bot.setPlayingGame(gamePlaying, function(error) {
      console.log(error.message);
    });
  }
  return;
}

function playRadio(message, params) {
  //search for radio station
  async.forEachOf(radio, function (value, key, callback) {
    if(value.command === params[1]){
      queue.push({
        title: value.name,
        user: message.author.username,
        url: value.url
      });
      bot.sendMessage(message.channel,'🎵`Added` 【**' + value.name +
       '**】 on pos `' + queue.length + '`');
      stopped = false;
      if(!stopped && queue.length !== 0 && !bot.voiceConnection.playing){
        playNextSong();
      }
    }
    callback();
  }, function (err) {
      if(err) {return console.log(err);}
  });
}

function playNextSong(){
  if(queue.length === 0) {
    bot.sendMessage(bot.channels.get('name', textChannelName),
     ':ok_hand: All done, playlist is empty!');
    bot.voiceConnection.stopPlaying();
    return;
  }
  if(!stopped && queue.length !== 0 && !bot.voiceConnection.playing){
    bot.voiceConnection.playFile(queue[0].url, function(error, playingIntent) {
      if(error) { console.log('Playback error: ' + error); }
      playingIntent.on('end', function() {
        if(!stopped) {
          playNextSong();
        }
      });
    });
  }
  bot.sendMessage(bot.channels.get('name', textChannelName),'🎵`Playing` 【**' +
   queue[0].title + '**】 `Volume: ' +
   bot.voiceConnection.getVolume()*100 + '%`');
  queue.splice(0,1);
}
