#!/usr/bin/env bash

apt-get update
apt-get install -y git curl ffmpeg build-essential
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.31.2/install.sh | bash
nvm install 4.4
npm install --prefix /vagrant
